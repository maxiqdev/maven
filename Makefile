.PHONY: gsp sap bigquery install

gsp:
	@echo 'Installing gsp jars...'
	mvn install:install-file\
	 -Dfile=gsp.jar\
	 -DgroupId=gsp\
	 -DartifactId=gsp\
	 -Dversion=1.0\
	 -Dpackaging=jar

sap:
	@echo 'Installing sap jars...'
	mvn install:install-file\
	 -Dfile=sap.jar\
	 -DgroupId=com.sap.conn.jco\
	 -DartifactId=com.sap.conn.jco.sapjco3\
	 -Dversion=3.0.17\
	 -Dpackaging=jar

bigquery:
	@echo 'Installing bigquery jars...'
	mvn install:install-file\
	 -Dfile=bigquery.jar\
	 -DgroupId=com.simba.bigquery\
	 -DartifactId=bigquery-connector\
	 -Dversion=1.0.0\
	 -Dpackaging=jar

install: gsp sap bigquery

